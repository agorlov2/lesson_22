// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lesson_22.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Lesson_22, "Lesson_22" );

DEFINE_LOG_CATEGORY(LogLesson_22)
 