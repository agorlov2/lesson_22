// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lesson_22GameMode.generated.h"

UCLASS(minimalapi)
class ALesson_22GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALesson_22GameMode();
};



