// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lesson_22GameMode.h"
#include "Lesson_22PlayerController.h"
#include "Lesson_22Character.h"
#include "UObject/ConstructorHelpers.h"

ALesson_22GameMode::ALesson_22GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ALesson_22PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}